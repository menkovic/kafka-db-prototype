package org.nmenkovic.microservices.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.concurrent.CountDownLatch;

public class KafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    private CountDownLatch latch = new CountDownLatch(1);

    @KafkaListener(topics = "${kafka.topic.rest-db}")
    public void receive(String message) {
        LOGGER.info("received message='{}'", message);
        // TODO: deserialize message, and call message handler
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
