package org.nmenkovic.microservices.repositories;

import org.nmenkovic.microservices.entities.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
